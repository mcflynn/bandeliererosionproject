__author__ = 'mark'
'''
Created on Sep 27, 2012

@author: Mark
'''

# Convert Landsat or Digital Globe image digital numbers into reflectance for NIR and R bands and then to NDVI
import sys
# import the GDAL and numpy libraries
from osgeo import gdal
from numpy import *
import math
import create_image
from import_landsat_imagery import *
import gdal_merge_me
from import_world_view_imagery import *
import argparse



# ***************************************************************
#
def calculate_ndvi(RED_ref, NIR_ref, image_date, reference_image, out_file_path, series_id=None):
    ndvi_diff = subtract(NIR_ref, RED_ref)
    ndvi_sum = add(NIR_ref, RED_ref)
    if series_id:
        output_ndvi_fname = os.path.join(out_file_path, 'NDVI_' + image_date + '_' + series_id + '.tiff')
    else:
        output_ndvi_fname = os.path.join(out_file_path, 'NDVI_' + image_date + '.tiff')
    ndvi = divide(ndvi_diff, ndvi_sum)
    create_image.create_image(reference_image, ndvi, output_ndvi_fname)


def calculate_qcal_radiance(RED, NIR, rad_max_array, rad_min_array, qcal_max_array, qcal_min_array):
    RED_radiance = (rad_max_array[0] - rad_min_array[0])/(qcal_max_array[0] - qcal_min_array[0]) \
                   * (RED - qcal_min_array[0]) + rad_min_array[0]
    NIR_radiance = (rad_max_array[1] - rad_min_array[1])/(qcal_max_array[1] - qcal_min_array[1]) \
                   * (NIR - qcal_min_array[1]) + rad_min_array[1]
    return RED_radiance, NIR_radiance


def calculate_radiance(RED, NIR, rad_mult_array, rad_add_array):

    RED_radiance = rad_mult_array[0] * RED + rad_add_array[0]
    NIR_radiance = rad_mult_array[1] * NIR + rad_add_array[1]
    return RED_radiance, NIR_radiance


def calculate_reflectance(RED_radiance, NIR_radiance, distance, solar_irradiance, sun_elev):

    RED_ref = (math.pi * RED_radiance * math.pow(distance,2))/(solar_irradiance[0] * math.cos(math.radians(sun_elev)))
    NIR_ref = (math.pi * NIR_radiance * math.pow(distance,2))/(solar_irradiance[1] * math.cos(math.radians(sun_elev)))
    return RED_ref, NIR_ref


def convert_date_to_julian(image_date):
    a = int((14-image_date.month)/12)
    y = image_date.year + 4800 - a
    m = image_date.month + 12*a - 3
    JDN = image_date.day + int((153*m + 2)/5) + 365*y + int(y/4.0) - int(y/100.0) + int(y/400.0) - 32045
    return JDN


def convert_landsat4_5_to_ndvi(in_file_path, out_file_path):
    rad_max_array, rad_min_array, qcal_min_array, qcal_max_array, fname_array = [],[],[],[], []
    bands = [3, 4]
    for band in bands:
        image_date, earth_sun_distance, rad_max, rad_min, qcal_min, qcal_max, sun_zenith, landsat_image \
            = import_landsat4_5(band, in_file_path)
        rad_max_array.append(rad_max)
        rad_min_array.append(rad_min)
        qcal_min_array.append(qcal_min)
        qcal_max_array.append(qcal_max)

        fname_array.append(landsat_image)

    band3 = os.path.join(in_file_path, fname_array[0])
    band4 = os.path.join(in_file_path, fname_array[1])

    distance = earth_sun_distance

    solar_irradiance = array([1558, 1047])

    g = gdal.Open(band4) # NIR band
    NIR = g.ReadAsArray()
    g = gdal.Open(band3) # Red band
    RED = g.ReadAsArray()

    # ****************************************************************

    sun_elev = 90-sun_zenith

    # NDVI = (nearInfrared - Red) / (nearInfrared + Red)

    # change the array data type from integer to float to allow decimals
    NIR = array(NIR, dtype=float)
    RED = array(RED, dtype=float)

    RED_radiance, NIR_radiance = calculate_qcal_radiance(RED, NIR, rad_max_array, rad_min_array, qcal_max_array, qcal_min_array)

    RED_ref, NIR_ref = calculate_reflectance(RED_radiance, NIR_radiance, distance, solar_irradiance, sun_elev)

    calculate_ndvi(RED_ref, NIR_ref, image_date, band3, out_file_path)


def convert_landsat7_to_ndvi(in_file_path, out_file_path):
    rad_mult_array, rad_add_array, ref_mult_array, ref_add_array, fname_array = [],[],[],[], []
    bands = [3,4]
    gap_mask_path = os.path.join(in_file_path, 'gap_mask')
    for band in bands:
        gap_mask_file = os.path.join(gap_mask_path, 'GM_B'+str(band)+'.shp')
        image_date,earth_sun_distance,rad_mult, rad_add, sun_zenith, landsat_image \
            = import_landsat7(band, in_file_path, gap_mask_file)
        rad_add_array.append(rad_add)
        rad_mult_array.append(rad_mult)

        fname_array.append(landsat_image)

    band3 = os.path.join(in_file_path, fname_array[0])
    band4 = os.path.join(in_file_path, fname_array[1])

    distance = earth_sun_distance

    solar_irradiance = array([1558,1047])

    g = gdal.Open(band4) # NIR band
    NIR = g.ReadAsArray()
    g = gdal.Open(band3) # Red band
    RED = g.ReadAsArray()

    # ****************************************************************

    sun_elev = 90-sun_zenith

    # NDVI = (nearInfrared - Red) / (nearInfrared + Red)

    # change the array data type from integer to float to allow decimals
    NIR = array(NIR, dtype=float)
    RED = array(RED, dtype=float)
    #

    RED_radiance, NIR_radiance = calculate_radiance(RED, NIR, rad_mult_array, rad_add_array)
    # ****************************************************************

    RED_ref, NIR_ref = calculate_reflectance(RED_radiance, NIR_radiance, distance, solar_irradiance, sun_elev)

    calculate_ndvi(RED_ref, NIR_ref, image_date, band3, out_file_path)


def convert_landsat8_to_ndvi(in_file_path, out_file_path):
    rad_mult_array, rad_add_array, ref_mult_array, ref_add_array, fname_array = [],[],[],[], []
    bands = [4,5]

    for band in bands:
        image_date, earth_sun_distance, ref_mult, rad_mult, rad_add, ref_mult, ref_add, sun_zenith, landsat_image \
            = import_landsat8(band, in_file_path)
        rad_add_array.append(rad_add)
        rad_mult_array.append(rad_mult)
        ref_add_array.append(ref_add)
        ref_mult_array.append(ref_mult)
        fname_array.append(landsat_image)

    band3 = os.path.join(in_file_path, fname_array[0])
    band4 = os.path.join(in_file_path, fname_array[1])

    distance = earth_sun_distance

    solar_irradiance = array([1558, 1047])

    g = gdal.Open(band4) # NIR band
    NIR = g.ReadAsArray()
    g = gdal.Open(band3) # Red band
    RED = g.ReadAsArray()

    # ****************************************************************

    sun_elev = 90-sun_zenith

    # change the array data type from integer to float to allow decimals
    NIR = array(NIR, dtype=float)
    RED = array(RED, dtype=float)

    RED_radiance, NIR_radiance = calculate_radiance(RED, NIR, rad_mult_array, rad_add_array)
    # ****************************************************************

    RED_ref, NIR_ref = calculate_reflectance(RED_radiance, NIR_radiance, distance, solar_irradiance, sun_elev)

    calculate_ndvi(RED_ref, NIR_ref, image_date, band3, out_file_path)


def create_dg_ndvi(in_file_path, img_name, meta_df, out_file_path, type, series_id=None):
    image_filename = os.path.join(in_file_path, img_name)
    img = gdal.Open(image_filename)
    img_array = img.ReadAsArray()
    RED = img_array[2]
    NIR = img_array[3]
    RED_rad = RED * meta_df['red_absCalFactor']/meta_df['red_effectiveBandWidth']
    NIR_rad = NIR * meta_df['nir_absCalFactor']/meta_df['nir_effectiveBandWidth']
    if type == 'worldview':
        red_solar_spectral_irradiance = 1559.4555
        nir_solar_spectral_irradiance = 1069.7302
    elif type == 'quickbird':
        red_solar_spectral_irradiance = 1574.77
        nir_solar_spectral_irradiance = 1113.71
    elif type == 'geoeye':
        red_solar_spectral_irradiance = 1505
        nir_solar_spectral_irradiance = 1039
    else:
        print "must specify satellite type"
        sys.exit(0)
    julian_day = convert_date_to_julian(meta_df['DATE_ACQUIRED'])
    D = julian_day - 2451545.0
    g = pi/180 * (357.529 + 0.98560028 * D)
    des = 1.00014 - 0.01671 * cos(g) - 0.00014 * cos(2*g)
    earth_sun_distance = get_earth_sun_distance(meta_df)
    RED_ref = (RED_rad * earth_sun_distance**2 * pi)/(red_solar_spectral_irradiance * cos(meta_df['solar_zenith_angle']))
    NIR_ref = (NIR_rad * earth_sun_distance**2 * pi)/(nir_solar_spectral_irradiance * cos(meta_df['solar_zenith_angle']))
    image_date = meta_df['DATE_ACQUIRED'].date().strftime("%Y-%m-%d")
    calculate_ndvi(RED_ref,NIR_ref, image_date, image_filename, out_file_path, series_id=series_id)


def convert_digital_globe_to_ndvi(in_file_path, img_filename, out_file_path, type):
    '''

    :param in_file_path: file path to image file
    :param img_filename: name of file
    :param out_file_path: filename of output file
    :param type: quickbird or worldview
    :return:
    '''


    meta_df = get_metadata(in_file_path)
    if isinstance(img_filename, list):
        filename1_split = img_filename[0].split('.')[0].split('_')
        sub_name1 = filename1_split[1].split('_')[0].split('-')[0]
        filename2_split = img_filename[1].split('.')[0].split('_')
        sub_name2 = filename2_split[1].split('_')[0].split('-')[0]
        output_file = filename1_split[0] + '_' + sub_name1 + '_' \
                      + sub_name2 + '_' + '_'.join(filename1_split[2:]) + '.tiff'
        argv = ['-n','-1','-init','NaN',
            '-o', os.path.join(in_file_path, output_file), os.path.join(in_file_path,img_filename[0]), os.path.join(in_file_path,img_filename[1])]
        gdal_merge_me.main(argv)

        create_dg_ndvi(output_file, meta_df, out_file_path, series_id=None)
    else:
        create_dg_ndvi(img_filename, meta_df,out_file_path)


def extend_raster(in_file_path, img_filename):
    output_file = img_filename.split('.')[0] + '_expanded' + '.tiff'
    argv = ['-n','-1','-init','NaN', '-tap',
            '-ul_lr', str(378894.5), str(3962167), str(386981.5), str(3958391),
            '-o', os.path.join(in_file_path, output_file), os.path.join(in_file_path,img_filename)]
    gdal_merge_me.main(argv)


if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Create NDVI image from satellite image', add_help=True)
    parser.add_argument('in_file_path',
                        help='Path to input images')
    parser.add_argument('out_file_path',
                        help='Path to where you want to output the NDVI image')
    parser.add_argument('--satellite_type', action='store', default='Landsat7',
                        help='Type of satellite image, acceptable values are Landsat4, Landsat7, Landsat8, DigitalGlobe')
    parser.add_argument('--dg_type', action='store', dest='type',
                        help='type of Digital Globe image, Worldview or Quickbird')

    args = parser.parse_args()

    if args.satellite_type == 'Landsat4':
        print "Creating NDVI image and saving to {0} from Landsat 4/5 images in {1}".format(args.out_file_path, args.in_file_path)
        convert_landsat4_5_to_ndvi(args.in_file_path, args.out_file_path)
    elif args.satellite_type == 'Landsat7':
        print "Creating NDVI image and saving to {0} from Landsat 7 images in {1}".format(args.out_file_path, args.in_file_path)
        convert_landsat7_to_ndvi(args.in_file_path, args.out_file_path)
    elif args.satellite_type == 'Landsat8':
        print "Creating NDVI image and saving to {0} from Landsat 8 images in {1}".format(args.out_file_path, args.in_file_path)
        convert_landsat8_to_ndvi(args.in_file_path, args.out_file_path)
    elif args.satellite_type == 'DigitalGlobe':
        print "Creating NDVI image and saving to {0} from DigitalGlobe {1} in {2}".format(args.out_file_path, args.dg_type, args.in_file_path)
        convert_digital_globe_to_ndvi(args.in_file_path, args.out_file_path, args.dg_type)
