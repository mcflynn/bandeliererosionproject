__author__ = 'mark'


# import the GDAL and numpy libraries
from osgeo import gdal,gdalnumeric
import numpy as np
import os.path
import datetime
from PIL import Image
import glob, os


def create_image(base,image_array,image_name,*args):
    #day of year of image
    #base is the image that you want the projection from

    g = gdal.Open(base)
    geo = g.GetGeoTransform()  # get the datum
    proj = g.GetProjection()   # get the projection
    xsize = g.RasterXSize
    ysize = g.RasterYSize
    if args:
        geo = args[0][0]
        sz = args[0][1]
        xsize = sz[1]
        ysize= sz[0]
#    band = g.GetRasterBand(1)
#    shape = np.array([band.YSize, band.XSize])        # get the image dimensions - format (row, col)
    driver = gdal.GetDriverByName('GTiff')
    dst_ds = driver.Create( image_name, xsize, ysize, 1, gdal.GDT_Float32)
    dst_ds.SetGeoTransform( geo ) # set the datum
    dst_ds.SetProjection( proj )  # set the projection
    try:
        dst_ds.GetRasterBand(1).WriteArray(image_array)
    except:
        print "failed WriteArray"
        print "array size: %d by %d" % image_array.shape
        print "image size %d by %d" % (xsize,ysize)
    stat = dst_ds.GetRasterBand(1).GetStatistics(1,1)  # get the band statistics (min, max, mean, standard deviation)
    dst_ds.GetRasterBand(1).SetStatistics(stat[0], stat[1], stat[2], stat[3]) # set the stats we just got to the band


def resize_images(image_dir,size):
    os.chdir(image_dir)
    for infile in glob.glob("*.jpg"):
        file, ext = os.path.splitext(infile)
        im = Image.open(infile)
        im.thumbnail(size, Image.ANTIALIAS)
        im.save(file + "_display", "JPEG")
