import os
import glob
import pandas as pd
import numpy as np
from gis_tools import subset_by_shapefile
from burn_raster import add_clouds
from datetime import datetime, date
import argparse


def get_ndvi(raster_path, input_csv, shapefile_path, cloud_shapefile_path, output_csv):
    """
    :param raster_path: path to input NDVI files
    :param input_csv: file that indicates if a NDVI image is too cloudy to use
    :param shapefile_path: path to ROI shapefiles
    :param cloud_shapefile_path: path to cloud mask shapefiles
    :return: dataframe with average NDVI for each ROI
    """

    os.chdir(raster_path)
    rows = []
    cloud_df = pd.read_csv(input_csv)
    for raster in glob.glob('*.tiff'):
        date_string = raster.split('_')[1].split('.')[0].split('-')
        im_date = map(int, date_string)
        image_date = date(im_date[0],im_date[1],im_date[2])
        print "starting {}".format(image_date)
        if cloud_df[cloud_df.image_date == '-'.join(date_string)].cloud_mask.values[0] != 'cloudy':
            # Image was NOT fully covered in clouds
            ndvi_masked_dir = os.path.join(raster_path, 'masked_dir')
            cloud_shapefile = os.path.join(cloud_shapefile_path, 'cloud_shape_'+'-'.join(date_string)+'.shp')
            raster_file = os.path.join(raster_path, raster)
            if os.path.exists(cloud_shapefile):
                # Image was partially covered by clouds and so those cloud covered portions are masked
                mask_file = raster.split('.')[0] + '_cloudmasked'
                ndvi_new_masked = os.path.join(ndvi_masked_dir, mask_file+".tif")
                add_clouds(raster_file, cloud_shapefile, ndvi_new_masked, 1)
                print "done cloudmasking for {}".format(image_date)
            else:
                ndvi_new_masked = raster_file

            os.chdir(shapefile_path)
            for shp_file in glob.glob('*.shp'):
                print shp_file
                clip_array = subset_by_shapefile(
                        ndvi_new_masked, shapefile_path, shp_file)
                pixel_count = float(np.count_nonzero(clip_array))
                nan_pixel_count = np.count_nonzero(np.isnan(clip_array))
                if nan_pixel_count/pixel_count > .5:
                    mean_ndvi = None
                else:
                    mean_ndvi = np.mean(clip_array)
                row = {'image_date': image_date, 'shape_name': shp_file, 'mean_ndvi': mean_ndvi}
                rows.append(row)
            os.chdir(raster_path)
            print "done {}".format(image_date)
        else:
            # Image had too much cloud cover and so can't be used
            print "skipping {}, too cloudy".format(image_date)
    ndvi_df = pd.DataFrame(rows)
    # ndvi_df = ndvi_df.pivot('image_date', 'shape_name')
    ndvi_df.to_csv(output_csv)
    # return ndvi_df

if __name__=='__main__':

    parser = argparse.ArgumentParser(description='Subset a series of NDVI images and calculate mean values for each image and ROI', add_help=True)
    parser.add_argument('raster_path',
                        help='Path to input NDVI images')
    parser.add_argument('input_csv',
                        help='Path to csv file listing dates and cloud cover')
    parser.add_argument('shapefile_path',
                        help='Path to shapefiles for ROIs')
    parser.add_argument('cloud_shapefile_path',
                        help='Path to cloud shapefiles')
    parser.add_argument('output_csv',
                        help='Path to where you want to output the csv summary file')

    args = parser.parse_args()

    get_ndvi(args.raster_path, args.input_csv, args.shapefile_path, args.shapefile_path, args.output_csv)

    # ndvi_df = get_ndvi('/home/mark/Devel/gis/gis/rasters/ndvi',
    #                    '/home/mark/Devel/gis/gis/image_dates_clouds.csv',
    #          # '/home/mark/Devel/gis/gis/rubble_shapefiles',
    #          # '/home/mark/Devel/gis/shapefiles/B16_exp_shapefiles',
    #                    '/home/mark/Devel/gis/shapefiles/BAND_2015_individual_shapefiles',
    #          '/home/mark/Devel/gis/gis/cloud_shapefiles')
    # ndvi_df.to_csv('/home/mark/Devel/gis/gis/ndvi_out_B2015_original_shapefiles.csv')