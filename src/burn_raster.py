'''
Created on Jan 24, 2013

@author: Owner
'''
from osgeo import ogr
from osgeo import gdal, gdalnumeric
import numpy as np
import os
import gdal_merge_me

def add_clouds(old_ndvi,cloud_shapefile,output_filename,band_num=1):

    aoi_raster = gdal.Open(old_ndvi)
    cloud_shapefile_dir = '/'.join(cloud_shapefile.split('/')[:-1])
    os.chdir(cloud_shapefile_dir)
    shape_datasource = ogr.Open(cloud_shapefile)
    shape_layer = shape_datasource.GetLayer()
    
    raster_out = 'new_raster.tif'
    
    cols = aoi_raster.RasterXSize
    rows = aoi_raster.RasterYSize
    projection = aoi_raster.GetProjection()
    geotransform = aoi_raster.GetGeoTransform()
    bands = aoi_raster.RasterCount
    
    driver = gdal.GetDriverByName('GTiff')
    
    success = False
    while not success:
        try:
            new_raster = driver.Create(str(raster_out), cols, rows, 1, gdal.GDT_Float32)
            new_raster.SetProjection(projection)
            new_raster.SetGeoTransform(geotransform)
    
            gdal.RasterizeLayer(new_raster, [1], shape_layer, burn_values=[-999.])
            success = True
        except:
#             print "retrying SetProjection"
            pass
    
    image_obj = new_raster.GetRasterBand(1)
    srcArray = image_obj.ReadAsArray()
#     neg_idx = np.flatnonzero(srcArray==0) 
#     srcArray.ravel()[neg_idx] = -999
    new_image_obj = aoi_raster.GetRasterBand(band_num)
    newArray = new_image_obj.ReadAsArray()
    newArray[np.isnan(newArray)] = -999
    output_array = newArray + srcArray
    output_array[output_array < -500] = np.nan
#     if os.path.exists(output_filename):
#         os.remove(output_filename)
    gdalnumeric.SaveArray(output_array, output_filename, format="GTiff", prototype=aoi_raster)
    new_raster = None
    aoi_raster=None
    os.remove(raster_out)
    #argv = ['-o',"new_raster_out.tif", raster_out,aoi_uri]
    #gdal_merge_me.main(argv)
def clip_shapefile(old_ndvi,whole_area_shapefile,output_filename,band_num):
    add_clouds(old_ndvi,whole_area_shapefile,output_filename,band_num)