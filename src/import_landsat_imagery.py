'''

'''

import os
import pandas as pd
import numpy as np
from datetime import datetime
from burn_raster import add_clouds
from gis_tools import *
from gdal_polygonize import *
import gzip
import math

def calc_earth_sun_distance(day):
    return 1.0 - 0.01672 * math.cos(0.9856*(day - 4))

def get_image_dates(rootdir):
    metadata_files = []
    for subdir, dirs, files in os.walk(rootdir):
        for fname in files:
            if 'MTL' in fname:
                metadata_files.append(os.path.join(subdir,fname))
    output = []
    for fname in metadata_files:
        dir_name = '/'.join(fname.split('/')[0:-1])
        file_name = fname.split('/')[-1]
        if 'LC8' in file_name:
            image_type = 'Landsat8'
        if 'LE7' in file_name:
            image_type = 'Landsat7'
        if 'LT' in file_name:
            image_type = 'Landsat4/5'
        os.chdir(dir_name)
        meta_df = get_metadata(dir_name)
        output.append({'image_name':dir_name.split('/')[-1], 'image_date':meta_df['DATE_ACQUIRED'], 'image_type':image_type})
    output_df = pd.DataFrame(output)
    output_df.to_csv('/home/mark/Devel/gis/gis/image_dates.csv')


def convert_gap_mask_to_shapefile(in_path, out_path, file_name):
    """
    Takes a reversed L7 gap tiff and converts to a shapefile
    :param in_path: path to tiff
    :param out_path: path to output shapefile
    :param file_name: name of tiff
    :return: gap shapefile
    """
    tiff_split = file_name.split('.')[0].split('_')
    band = tiff_split[3]
    shapefile = tiff_split[2]
    reversed_tiff = reverse_gap_mask(os.path.join(in_path, file_name))
    output_shapefile = os.path.join(in_path, shapefile+'_'+band+'_rev.shp')
    gap_shapefile = polygonize(reversed_tiff,
                     output_shapefile,
                     'GM',
                     mask=reversed_tiff,
                     format="ESRI Shapefile",
                     out_file=output_shapefile)


    return output_shapefile


def reverse_gap_mask(tiff):
    """
    Takes gap tiff and converts 0's to 1's and vice versa
    :param tiff: name of input tiff
    :return: output tiff
    """
    srcArray = gdalnumeric.LoadFile(tiff)
    srcArray[srcArray == 0] = -1
    srcArray[srcArray == 1] = 0
    srcArray[srcArray == -1] = 1
    im = arrayToImage(srcArray)
    new_tiff = tiff.split('.')[0] + '_reversed.TIF'
    gdalnumeric.SaveArray(srcArray, new_tiff, format="GTiff", prototype=tiff)
    return new_tiff


def get_metadata(dpath):
    dir_changed = False
    if os.getcwd() != dpath:
        dir_changed = True
        old_cwd = os.getcwd()
        os.chdir(dpath)

    for metadata_file in glob.glob('*MTL.txt'):
        # get metadata file from the working directory

        f = open(metadata_file)

        contents = f.read()
        lines = contents.split('\n')
        linest = [x.strip() for x in lines]
        groups = [x.strip().split(' = ') for x in linest]

        # append a NULL to any row with length less than 2 (like 'END')
        for idx,row in enumerate(groups):
            if len(row) < 2:
                groups[idx].append('NULL')

        group_tuples = [tuple(x) for x in groups]
        group_dict = dict(group_tuples)
        meta_df = pd.Series(group_dict)
        if dir_changed is True:
            os.chdir(old_cwd)
        return meta_df


def get_earth_sun_distance(meta_df):

    # get earth sun distance from file and fill in missing values by interpolating
    image_date = meta_df['DATE_ACQUIRED']
    if not isinstance(image_date, datetime):
        date_tuple = map(int, image_date.split('-'))
        image_datetime = datetime(date_tuple[0],date_tuple[1],date_tuple[2]).date()
    else:
        image_datetime = image_date
    doy = image_datetime.timetuple().tm_yday
    # earth_sun_distance = pd.DataFrame(columns=['DOY', 'Distance'])
    # earth_sun_distance['DOY'] = np.arange(1, 366)
    # earth_sun_distance['Distance'] = [calc_earth_sun_distance(x) for x in np.arange(1,366)]
    # esun_df = [calc_earth_sun_distance(x) for x in np.arange(1,366)]
    # df_merge = pd.merge(esun_doy_df,esun_df,how='left',left_on='DOY',right_on='DOY')
    # df_merge = df_merge.interpolate()
    # earth_sun_distance = df_merge[df_merge.DOY==doy].Distance_y.values[0]
    return calc_earth_sun_distance(doy)


def get_sun_zenith(meta_df):
    SunElevAll = meta_df['SUN_ELEVATION']
    sun_zenith = (90.0 - float(SunElevAll))
    return sun_zenith


def import_landsat4_5(band_num, dpath):
    os.chdir(dpath)
    meta_df = get_metadata(dpath)
    image_date = meta_df['DATE_ACQUIRED']

    earth_sun_distance = get_earth_sun_distance(meta_df)
    sun_zenith = get_sun_zenith(meta_df)
    rad_max = float(meta_df['RADIANCE_MAXIMUM_BAND_{0}'.format(band_num)])
    rad_min = float(meta_df['RADIANCE_MINIMUM_BAND_{0}'.format(band_num)])
    qcal_min = float(meta_df['QUANTIZE_CAL_MIN_BAND_{0}'.format(band_num)])
    qcal_max = float(meta_df['QUANTIZE_CAL_MAX_BAND_{0}'.format(band_num)])
    landsat_image = meta_df['FILE_NAME_BAND_{}'.format(band_num)].strip('"')

    return image_date, earth_sun_distance, rad_max, rad_min, qcal_min, qcal_max, sun_zenith, landsat_image


def import_landsat7(band_num, dpath, gap_mask_file):
    """
    :param band_num: number of band from landsat image
    :param dpath: path to raster files
    :param gap_shapefile: shapefile used to mask no-data stripes
    :return:
    """

    os.chdir(dpath)
    meta_df = get_metadata(dpath)
    image_date = meta_df['DATE_ACQUIRED']

    earth_sun_distance = get_earth_sun_distance(meta_df)
    sun_zenith = get_sun_zenith(meta_df)

    rad_mult = float(meta_df['RADIANCE_MULT_BAND_{0}'.format(band_num)])
    rad_add = float(meta_df['RADIANCE_ADD_BAND_{0}'.format(band_num)])

    landsat_image_filename = meta_df['FILE_NAME_BAND_{}'.format(band_num)].strip('"')
    # landsat_image = os.path.join(dpath,"NDVI_"+image_date+".tif")

    masked_tif_dir = os.path.join(dpath,'masked_dir')
    gap_mask_dir = os.path.join(dpath, 'gap_mask')

    # Landsat7 tiff for masking
    mask_file = '_'.join(landsat_image_filename.split('_')[0:7])+'_GM_'+'B'+str(band_num)+'.TIF'
    if not os.path.exists(os.path.join(gap_mask_dir, mask_file)):
        os.system("gunzip {}".format(os.path.join(gap_mask_dir, mask_file)))

    # make masked_dir if it doesn't exist
    if not os.path.exists(masked_tif_dir):
        os.mkdir(masked_tif_dir)

    # filename of tiff that's had gap masking
    gap_masked_file = os.path.join(os.path.join(dpath, 'masked_dir'),
                                   mask_file.split('.')[0]+"_gapmasked.tif")

    # create gapmask shapefile
    gap_shapefile = convert_gap_mask_to_shapefile(
            os.path.join(dpath, 'gap_mask'),
            os.path.join(dpath, 'gap_mask'),
            os.path.join(gap_mask_dir, mask_file))

    # masking no-data stripes if gap-masked file doesn't exist
    if not os.path.exists(gap_masked_file):
        add_clouds(landsat_image_filename, gap_shapefile, gap_masked_file, 1)

    return image_date, earth_sun_distance, rad_mult, rad_add, sun_zenith, gap_masked_file


def import_landsat8(band_num, dpath):
    '''
    User enters working directory. Metadata file opened.***********************
    '''
    os.chdir(dpath)#set working directory
    meta_df = get_metadata(dpath)

    '''
    Get calibration data to convert digital numbers to reflectance.************
    '''

    #Find gain for all bands and save NIR (b2) and red (b3) gain
    #values as variables (eval format)
    image_date = meta_df['DATE_ACQUIRED']
    earth_sun_distance = float(meta_df['EARTH_SUN_DISTANCE'])
    sun_zenith = get_sun_zenith(meta_df)
    landsat_image = meta_df['FILE_NAME_BAND_{}'.format(band_num)].strip('"')

    rad_mult = float(meta_df['RADIANCE_MULT_BAND_{0}'.format(band_num)])
    rad_add = float(meta_df['RADIANCE_ADD_BAND_{0}'.format(band_num)])
    ref_mult = float(meta_df['REFLECTANCE_MULT_BAND_{0}'.format(band_num)])
    ref_add = float(meta_df['REFLECTANCE_ADD_BAND_{0}'.format(band_num)])

    '''
    Open Red and NIR images from the working directory.*************************
    '''
    fname = os.path.join(dpath,"NDVI_"+image_date+".tif")
    if os.path.exists(fname):
        os.remove(fname)

    return image_date, earth_sun_distance, ref_mult, rad_mult, rad_add, ref_mult, ref_add, sun_zenith, landsat_image

if __name__=='__main__':
    # out_file_path = '/home/mark/Devel/gis/gis/rasters/landsat/2009-138/gap_mask/masked_dir'
    # in_file_path = '/home/mark/Devel/gis/gis/rasters/landsat/2009-138/gap_mask'
    # convert_gap_mask_to_shapefile(in_file_path, out_file_path, 'LE70330352009138EDC00_GM_B4.TIF')
    in_file_path = '/home/mark/Devel/gis/gis/rasters/landsat'
    get_image_dates(in_file_path)