import operator

from import_landsat_imagery import *
from osgeo import gdal, gdalnumeric, ogr, osr
import os
import tarfile
import glob
import pandas as pd
# from osgeo import gdal, gdalnumeric, ogr
from PIL import Image, ImageDraw


def imageToArray(i):
    """
    Converts a Python Imaging Library array to a gdalnumeric image.
    """
    a=gdalnumeric.fromstring(i.tostring(),'b')
    a.shape=i.im.size[1], i.im.size[0]
    return a


def arrayToImage(a):
    """
    Converts a gdalnumeric array to a Python Imaging Library Image.
    """
    i=Image.frombytes('L',(a.shape[1],a.shape[0]),
            (a.astype('b')).tostring())
    return i


def world2Pixel(geoMatrix, x, y):
    """
    Uses a gdal geomatrix (gdal.GetGeoTransform()) to calculate
    the pixel location of a geospatial coordinate 
    """
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
#    yDist = geoMatrix[5]
#    rtnX = geoMatrix[2]
#    rtnY = geoMatrix[4]
    pixel = int((x - ulX) / xDist)
    line = int((ulY - y) / xDist)
    return pixel, line


def histogram(a, bins=range(0,256)):
    """
    Histogram function for multi-dimensional array.
    a = array
    bins = range of numbers to match 
    """
    fa = a.flat
    n = gdalnumeric.searchsorted(gdalnumeric.sort(fa), bins)
    n = gdalnumeric.concatenate([n, [len(fa)]])
    hist = n[1:]-n[:-1] 
    return hist


def stretch(a):
    """
    Performs a histogram stretch on a gdalnumeric array image.
    """
    hist = histogram(a)
    im = arrayToImage(a)   
    lut = []
    for b in range(0, len(hist), 256):
        # step size
        step = reduce(operator.add, hist[b:b+256]) / 255
        # create equalization lookup table
        n = 0
        for i in range(256):
            lut.append(n / step)
            n = n + hist[i+b]
    im = im.point(lut)
    return imageToArray(im)


def get_geotransforms(in_path, out_file):
    """
    get geotransforms for all images in a Landsat imageset
    :param in_path: root directory of files you want geotransforms for
    :param out_file: output file
    :return:
    """
    rows = []
    for subdir, dirs, files in os.walk(in_path):
        for filename in files:
            if '_B1' in filename and filename.endswith('.TIF'):
                meta_df = get_metadata(subdir)
                print filename
                srcImage = gdal.Open(os.path.join(subdir,filename))
                geoTrans = srcImage.GetGeoTransform()
                row = {'image_date': meta_df['DATE_ACQUIRED'], 'ulX': geoTrans[0], 'ulY': geoTrans[3], 'xDist': geoTrans[1]}
                rows.append(row)

    geo_trans_df = pd.DataFrame(rows)
    geo_trans_df.to_csv(out_file)


def subset_by_shapefile(raster, shapefile_dir, shp, output=None):
    # Load the source data as a gdalnumeric array
    srcArray = gdalnumeric.LoadFile(raster)

    # Also load as a gdal image to get geotransform (world file) info
    srcImage = gdal.Open(raster)
    geoTrans = srcImage.GetGeoTransform()

    # change to the current working directory to where the shapefile is located:
    if os.path.isdir(shapefile_dir):
        os.chdir(shapefile_dir)
    else:
        print "directory doesn't exist"
        return
    if shp.endswith('.shp'):
        shp = shp.split('.')[0]
    # Create an OGR layer from a Field boundary shapefile
    if os.path.isfile(shp + ".shp"):
        field = ogr.Open("%s.shp" % shp)
    else:
        print "file doesn't exist"
        return

    lyr = field.GetLayer(shp)
    poly = lyr.GetNextFeature()
    
    # Convert the layer extent to image pixel coordinates
    minX, maxX, minY, maxY = lyr.GetExtent()
    ulX, ulY = world2Pixel(geoTrans, minX, maxY)
    lrX, lrY = world2Pixel(geoTrans, maxX, minY)
    
    # Calculate the pixel size of the new image
    pxWidth = int(lrX - ulX) + 1
    pxHeight = int(lrY - ulY) + 1
    
    src_clip = srcArray[ulY:lrY+1, ulX:lrX+1]
    
    # Create a new geomatrix for the image
    geoTrans = list(geoTrans)
    geoTrans[0] = minX
    geoTrans[3] = maxY
    
    # Map points to pixels for drawing the field boundary on a blank
    # 8-bit, black and white, mask image.
    points = []
    pixels = []
    geom = poly.GetGeometryRef()
    pts = geom.GetGeometryRef(0)
    for p in range(pts.GetPointCount()):
        points.append((pts.GetX(p), pts.GetY(p)))
    for p in points:
        pixels.append(world2Pixel(geoTrans, p[0], p[1]))
    rasterPoly = Image.new("L", (pxWidth, pxHeight), 1)
    rasterize = ImageDraw.Draw(rasterPoly)
    rasterize.polygon(pixels, 0)
    mask = imageToArray(rasterPoly)   
    
    # Clip the image using the mask
    clip = gdalnumeric.choose(mask, (src_clip, -1)).astype(gdalnumeric.uint8)

    if output:
        # Save ndvi as tiff
        gdalnumeric.SaveArray(clip, "%s.tif" % output, format="GTiff", prototype=raster)

        # Save ndvi as an 8-bit jpeg for an easy, quick preview
        clip = clip.astype(gdalnumeric.uint8)
        gdalnumeric.SaveArray(clip, "%s.tif" % output, format="GTiff")
    else:
        return src_clip


def reproject_layer(in_crs, out_crs, shapefile):
    driver = ogr.GetDriverByName('ESRI Shapefile')

    # input SpatialReference
    inSpatialRef = osr.SpatialReference()
    inSpatialRef.ImportFromEPSG(in_crs)

    # output SpatialReference
    outSpatialRef = osr.SpatialReference()
    outSpatialRef.ImportFromEPSG(out_crs)

    # create the CoordinateTransformation
    coordTrans = osr.CoordinateTransformation(inSpatialRef, outSpatialRef)

    # get the input layer
    shapefile_split = shapefile.split('/')
    shp = shapefile_split[-1]
    shp_dir = '/'.join(shapefile_split[:-1])
    os.chdir(shp_dir)

    inDataSet = driver.Open(shp)
    inLayer = inDataSet.GetLayer()

    # create the output layer
    outputShapefile = os.path.join(shp_dir, shp.split('.')[0] + '_' + str(out_crs) + '.shp')
    if os.path.exists(outputShapefile):
        driver.DeleteDataSource(outputShapefile)
    outDataSet = driver.CreateDataSource(outputShapefile)
    outLayer = outDataSet.CreateLayer("R", geom_type=ogr.wkbMultiPolygon)

    # add fields
    inLayerDefn = inLayer.GetLayerDefn()
    for i in range(0, inLayerDefn.GetFieldCount()):
        fieldDefn = inLayerDefn.GetFieldDefn(i)
        outLayer.CreateField(fieldDefn)

    # get the output layer's feature definition
    outLayerDefn = outLayer.GetLayerDefn()

    # loop through the input features
    inFeature = inLayer.GetNextFeature()
    while inFeature:
        # get the input geometry
        geom = inFeature.GetGeometryRef()
        # reproject the geometry
        geom.Transform(coordTrans)
        # create a new feature
        outFeature = ogr.Feature(outLayerDefn)
        # set the geometry and attribute
        outFeature.SetGeometry(geom)
        for i in range(0, outLayerDefn.GetFieldCount()):
            outFeature.SetField(outLayerDefn.GetFieldDefn(i).GetNameRef(), inFeature.GetField(i))
        # add the feature to the shapefile
        outLayer.CreateFeature(outFeature)
        # destroy the features and get the next input feature
        outFeature.Destroy()
        inFeature.Destroy()
        inFeature = inLayer.GetNextFeature()
    create_esri_prj_file(out_crs, shp)
    # close the shapefiles
    inDataSet.Destroy()
    outDataSet.Destroy()

def create_esri_prj_file(out_crs, out_shapefile):
    spatialRef = osr.SpatialReference()
    spatialRef.ImportFromEPSG(out_crs)

    spatialRef.MorphToESRI()
    prj_file = open(out_shapefile.split('.')[0] + '.prj', 'w')
    prj_file.write(spatialRef.ExportToWkt())
    prj_file.close()

def create_test_tiff():
    shp = 'BAND_2016_project_area.shp'
    shp_dir = '/home/mark/Devel/gis/shapefiles'
    raster = '/home/mark/Devel/gis/gis/rasters/digital_globe/055514544010_01/055514544010_01_P001_MUL/11JUL08172537-M2AS_R1C1-055514544010_01_P001.TIF'
    os.chdir(shp_dir)
    subset_by_shapefile(raster,shp_dir,shp,output='/home/mark/Devel/gis/gis/tests/fixtures/DG/test_DG')


def untar_landsat_image(in_path, base_out_path):
    """
    utility function to uncompress a bunch of landsat images
    :param in_path:
    :param base_out_path:
    :return:
    """
    os.chdir(in_path)
    for landsat_image in glob.glob('*.tar.gz'):
        # LT50330352008192PAC02.tar.gz
        ls_split = landsat_image.split('.')
        out_path = os.path.join(base_out_path,ls_split[0][9:13] + '-' + ls_split[0][13:16])
        if not os.path.exists(out_path):
            # /home/mark/Devel/gis/gis/rasters/landsat
            os.mkdir(out_path)
            tar = tarfile.open(landsat_image)
            tar.extractall(path=out_path)
        else:
            print "outpath: {} already exists".format(out_path)

if __name__=='__main__':
    # src_file = '/home/mark/Devel/gis/gis/rasters/landsat/2009-138/LE70330352009138EDC00_B3.TIF'
    # shape_dir = '/home/mark/Devel/gis/gis/tests'
    # shp = 'gap_mask_test.shp'
    # subset_by_shapefile(src_file, shape_dir, shp, output=True)
    # reproject_layer(26913, 32613, '/home/mark/Devel/gis/shapefiles/new_shapefiles/BAND_2016CESU_site_boundaries_OBJECTID_1.shp')

    # untar_landsat_image('/home/mark/bda/Bulk Order 710137/L7 ETM_ SLC-off _2003-present_',
    #                     '/home/mark/Devel/gis/gis/rasters/landsat')
    get_geotransforms('/home/mark/Devel/gis/gis/rasters/landsat', '/home/mark/Devel/gis/gis/landsat_geotransforms.csv')
