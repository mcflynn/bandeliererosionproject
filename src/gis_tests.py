__author__ = 'mark'

import unittest
from gis_meetup import subset_by_shapefile
from Landsat_to_NDVI import convert_landsat7_to_ndvi
import os

class gis_test(unittest.TestCase):
    def setUp(self):
        self.base_dir = os.path.join(os.path.basename(os.path.dirname(os.path.realpath(__file__))))
        self.raster = os.path.join(self.dir, 'rasters', 'LE07_L1TP_033035_20090526_20160917_01_T1')
        self.output_ndvi = 'NDVI.tif'
        self.shapefile_dir = os.path.join(self.raster, 'rubble_shapefiles')
        self.shapefile = 'corn_field'
        self.output = os.path.join(self.dir, 'output','corn')
        self.mask_value = None


    def test_ndvi(self):
        # os.remove(self.output_ndvi)
        convert_landsat7_to_ndvi(self.raster, self.output_ndvi)
        self.assertTrue(os.path.exists(os.path.join(self.dir, 'rasters','NDVI.tif')))

    def test_subset(self):
        subset_by_shapefile(self.raster, self.shapefile_dir, self.shapefile,self.output)
        self.assertTrue(os.path.exists(os.path.join(self.dir, 'output','corn.tif')))

if __name__ == '__main__':
    unittest.main()
