# Bandelier Erosion Project #

## What is this repository for?
Code written to analyze satellite images for Bandelier Erosion Project. This repository can be used by anyone who needs
to create NDVI images from Landsat 4/5, Landsat 7 and Landsat 8 images. The resulting NDVI images can then be further
analyzed by subsetting ROI's. The NDVI images can be directly compared regardless of which type of satellite image was
used to generate it.

## How do I get set up?

This works with Anaconda

```
conda create --name gis python=2.7
pip install -r requirements.py
conda install gdal
```
## Running the tests

To run tests:
```
cd src\
python tests.py
```

### Break down into end to end tests

Tests were created for:
    reading metadata files
    calculating radiance, reflectance and NDVI
    for Landsat 4 and Landsat 7 images



## Project Description

###Turns Landsat images into NDVI images
Takes a Landsat 4/5, 7 or 8 image and cloud masks image by creating a new image with all pixels covered by the cloud
shape file converted to NaN's. Landsat 7 images have a problem where the images have bands of no data. Because of this,
they go through an additional step where the gap mask file included with the image package is applied so that the
final image has the pixels in these no-data stripes converted to NaN's.

1) Download Landsat image(s)
 https://earthexplorer.usgs.gov/

2) unpack into directory of choice (e.g. [PROJECT_ROOT]/rasters)

3) run
```angular2html
    python Satellite_to_NDVI.py '/Devel/bandeliererosionproject/rasters/LE07_L1TP_033035_20090518_20160917_01_T1' '/Devel/bandeliererosionproject/rasters/ndvi' --satellite_type='Landsat7'
```
###Calculate mean NDVI 
Create a shapefile for each region of interest. Each shapefile is used to clip out a portion of the image
and the mean value for all real values (not NaN) in this array is calculated.
Inputs:
    1) Directory with NDVI images
    2) CSV file with list of image dates, amount of cloud cover, type of satellite image, UTM coordinates of upper left
       corner, size of pixels in meters
    3) Directory with ROI shapefiles
    4) Directory with cloud shapefiles
    5) Output filename
```angular2html
 python analyze_ndvi_rois.py '/Devel/bandeliererosionproject/rasters/ndvi' '/Devel/bandeliererosionproject/cloud_shapefiles/cloud_image_dates.csv' '/Devel/bandeliererosionproject/rubble_shapefiles' '/Devel/bandeliererosionproject/cloud_shapefiles' '/Devel/bandeliererosionproject/output/NDVI_summary.csv'
```